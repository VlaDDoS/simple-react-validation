import { useEffect, useState } from 'react';
import './App.css';

export const App = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const [emailDirty, setEmailDirty] = useState(false);
  const [passwordDirty, setPasswordDirty] = useState(false);

  const [emailError, setEmailError] = useState('Email is not empty');
  const [passwordError, setPasswordError] = useState('Password is not empty');

  const [formValid, setFormValid] = useState(false);

  const blurHandler = (e) => {
    switch (e.target.name) {
      case 'email':
        setEmailDirty(true);
        break;

      case 'password':
        setPasswordDirty(true);
        break;
    }
  };

  const emailHandler = (e) => {
    setEmail(e.target.value);

    const regExp =
      /^([0-9a-zA-Z]([-\.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$/;

    if (!regExp.test(String(e.target.value).toLocaleLowerCase())) {
      setEmailError('Incorrect Email');
    } else {
      setEmailError('');
    }
  };

  const passwordHandler = (e) => {
    setPassword(e.target.value);

    if (e.target.value.length < 3 || e.target.value.length > 8) {
      setPasswordError(
        'the password must be at least 3 and no more than 8 characters'
      );

      if (!e.target.value) {
        setPasswordError('Password is not empty');
      }
    } else {
      setPasswordError('');
    }
  };

  useEffect(() => {
    if(emailError || passwordError) {
      setFormValid(false);
    } else {
      setFormValid(true);
    }
  }, [emailError, passwordError])

  return (
    <div className="app">
      <form>
        <h1>Registration</h1>

        {emailDirty && emailError && <div className="error">{emailError}</div>}
        <input
          onChange={emailHandler}
          onBlur={blurHandler}
          value={email}
          name="email"
          type="text"
          placeholder="Enter your email..."
        />

        {passwordDirty && passwordError && (
          <div className="error">{passwordError}</div>
        )}
        <input
          onChange={passwordHandler}
          onBlur={blurHandler}
          value={password}
          name="password"
          type="password"
          placeholder="Enter your password..."
        />

        <button disabled={!formValid} type="submit">Registration</button>
      </form>
    </div>
  );
};
